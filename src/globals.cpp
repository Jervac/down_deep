#include "globals.h"

const int WINDOW_WIDTH = 1280;
const int WINDOW_HEIGHT = 720;
const std::string WINDOW_TITLE = "Game";
const int TILE_SIZE = 64;
