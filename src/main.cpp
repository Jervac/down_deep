#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include "globals.h"
#include "utils.h"
#include "math.h"
#include "Chronometer.h"

sf::RenderWindow window;
sf::View camera;

// Dimension of dungeon in rooms
int DUNGEON_WIDTH  = 3;
int DUNGEON_HEIGHT = 3;

// Size of Room in Tiles
int ROOM_SIZE = 12;

void init();
void input();
void update();
void render();

struct Entity {
    Entity() {};
    Entity(sf::Vector2f nposition) { // TODO: look in to changing how this is done
        position = nposition;
    };

    sf::Vector2f position;
    sf::Vector2f size;
    float velocity = 6.34f;
    bool alive = true;
    int hp = 3;
    bool invincible = false;

    // Variables for when hit
    bool being_hurt = false;
    sf::Vector2f target_hurt_position;
    sf::Vector2f hurt_vector; // normalized already
    float knock_back = TILE_SIZE * 3;
    float knock_back_velocity = velocity * 1.4;

    virtual void update() = 0;
    virtual void render(sf::RenderWindow &window) = 0;

    Rect bounds() {
        return Rect(position.x, position.y, size.x, size.y);
    };

    Rect getBounds() {
        return Rect(position.x, position.y, size.x, size.y);
    };

    sf::Vector2f getPosition() {
        return position;
    };

    sf::Vector2f getSize() {
        return size;
    };

    // Moves enemy in direction it's hit in
    void hurt(int damage, float dx, float dy) {
        hp -= damage;
        hurt_vector.x = dx;
        hurt_vector.y = dy;
        target_hurt_position.x = position.x + (dx * knock_back);
        target_hurt_position.y = position.y + (dy * knock_back);
        being_hurt = true;
    };
};

struct Wall : public Entity {
    Wall(sf::Vector2f nposition)
        : Entity (nposition){
        size.x = TILE_SIZE;
        size.y = TILE_SIZE;
        texture.loadFromFile("assets/tiles/wall.png");
        sprite.setTexture(texture);
        sprite.setPosition(position);
        sprite.setScale(size.x / sprite.getGlobalBounds().width, size.y / sprite.getGlobalBounds().height);
    };

    sf::Sprite sprite;
    sf::Texture texture;

    void update() {

    };
    void render(sf::RenderWindow &window) {
        sf::RectangleShape shape;
        shape.setPosition(position);
        shape.setSize(size);
        shape.setFillColor(sf::Color::White);
        window.draw(shape);

        sprite.setPosition(position);
        window.draw(sprite);
    };
};

struct Room : public Entity {
    Room(int x, int y, int w, int h) {
        position.x = x;
        position.y = y;
        size.x = w;
        size.y = h;

        // put walls around room edges
        // left and right
        for (int j=0; j<ROOM_SIZE; j++) {
            int middle_x = (int) (floor(ROOM_SIZE/2));
            if (j != middle_x && j != middle_x - 1) {
                walls.push_back(new Wall(sf::Vector2f(getPosition().x,
                                                      getPosition().y + (j * TILE_SIZE))));
                walls.push_back(new Wall(sf::Vector2f(getPosition().x + getSize().x - TILE_SIZE,
                                                      getPosition().y + (j * TILE_SIZE))));
            }
        }

        // up and down
        int middle_y = (int) (floor(ROOM_SIZE/2));
        for (int j=0; j<ROOM_SIZE; j++) {
            if (j != middle_y && j!= middle_y - 1) {
                walls.push_back(new Wall(sf::Vector2f(getPosition().x + (j * TILE_SIZE),
                                                      getPosition().y)));
                walls.push_back(new Wall(sf::Vector2f(getPosition().x + (j * TILE_SIZE),
                                                      getPosition().y + getSize().y - TILE_SIZE)));
            }
        }
    };

    std::vector<Wall*> walls;

    void update() {

    };

    void render(sf::RenderWindow &window) {
        sf::RectangleShape shape;
        shape.setFillColor(sf::Color(140, 140, 140, 255));
        shape.setSize(size);
        shape.setPosition(position);
        window.draw(shape);

        for (auto w : walls) {
            w->render(window);
        }
    };
};

struct Player : public Entity {
    Player(sf::Vector2f nposition)
        : Entity(nposition) {
        size.x = TILE_SIZE/1.2f;
        size.y = TILE_SIZE/1.2f;

		hurt_timer.reset(true);
    };

    float dx = 0, dy = 0;
    sf::Vector2f target_position;
    float attack_size = 80;
    float attack_distance = TILE_SIZE * 1.4f;

	sftools::Chronometer hurt_timer;
	float hurt_delay = 1;

    void update() {
		if (being_hurt && hurt_timer.getElapsedTime().asSeconds() >= hurt_delay) {
			being_hurt = false;
			hurt_timer.reset(false);
		}

        //camera.setCenter(camera.getCenter().x + (target_position.x-camera.getCenter().x)*.1f,
       //                 camera.getCenter().y + (target_position.y-camera.getCenter().y)*.1f);

        camera.setCenter(camera.getCenter().x + (position.x-camera.getCenter().x)*.1f,
                        camera.getCenter().y + (position.y-camera.getCenter().y)*.1f);

        sf::Vector2f mouse_world = window.mapPixelToCoords(sf::Mouse::getPosition(window));

        float dx = mouse_world.x - getCenterPosition().x, dy = mouse_world.y - getCenterPosition().y;
        float len = sqrt((dx*dx)+(dy*dy));
        if (len != 0) {
            dx /= len;
            dy /= len;
        }

        target_position = sf::Vector2f(getCenterPosition().x + (dx * attack_distance) - (attack_size/2),
                                       getCenterPosition().y + (dy * attack_distance) - (attack_size/2));
    };

    void render(sf::RenderWindow &window) {
        sf::RectangleShape shape;
        shape.setFillColor(sf::Color::Cyan);
		if (being_hurt) shape.setFillColor(sf::Color::Red);
        shape.setPosition(position);
        shape.setSize(size);
        window.draw(shape);

        sf::RectangleShape shot_direction_shape;
     //   shot_direction_shape.setPosition(sf::Vector2f(target_position.x,
       //                                               target_position.y));
       // shot_direction_shape.setSize(sf::Vector2f(attack_size, attack_size));
		shot_direction_shape.setPosition(sf::Vector2f(target_position.x + (size.x*1.5f) - (attack_size/2), target_position.y + (size.y*1.5f) - (attack_size/2)));
		shot_direction_shape.setSize(sf::Vector2f(10, 10));
        shot_direction_shape.setFillColor(sf::Color(200, 70, 30));
        window.draw(shot_direction_shape);
    };

    sf::Vector2f getCenterPosition() {
        return sf::Vector2f(position.x + (size.x/2), position.y + (size.y/2));
    };
};

Player *player;

struct Enemy : public Entity {
    Enemy(sf::Vector2f nposition) {
        position = nposition;
        size.x = TILE_SIZE/1.2f;
        size.y = TILE_SIZE/1.2f;
        redirection_timer.reset(true);
        velocity = 4.2f;
        hp = 5;
    };
    Enemy(sf::Vector2f nposition, std::string ntype) {
        position = nposition;
		type = ntype;
        size.x = TILE_SIZE/1.2f;
        size.y = TILE_SIZE/1.2f;
        redirection_timer.reset(true);
		if (type == "shooter") {
			velocity = 2.2f;
			hp = 8;
			shot_timer.reset(true);
		}
    };

	std::string type = "rat";
    int direction = 1; // 1 - 4

    sftools::Chronometer redirection_timer;
    float redirection_delay = (float) (rand(1, 3));

	// shooter
	sftools::Chronometer shot_timer;
	float shot_delay = 2.4f;
	bool player_in_range = false;

	Rect *shoot_range;
	bool shooting = false;

    void update() {
        if (hp <= 0) alive = false;

		shoot_range = new Rect(position.x - 400, position.y - 400, 800, 800);

		// shooter
		if (!shooting && player_in_range && shot_timer.getElapsedTime().asSeconds() >= shot_delay) {
			shooting = true;
		}

        if (!being_hurt && redirection_timer.getElapsedTime().asSeconds() >= redirection_delay) {
            while (true) {
                int target_direction = rand(1,4);
                if (target_direction != direction) {

                    // Choose axis to attmept to move towards player on
                    bool x_axis = (abs(player->getPosition().x - position.x) >
                                   abs(player->getPosition().y - position.y));

                    if (x_axis) direction = rand(3,4);
                    else direction = rand(1,2);
                    redirection_delay = (float) (rand(1, 3));
                    redirection_timer.reset(true);
                    break;
                }
            }
        }
    };

    void render(sf::RenderWindow &window) {
        sf::RectangleShape shape;
        shape.setFillColor(sf::Color::Red);
		if (type == "shooter") {
			shape.setFillColor(sf::Color::Green);
			if (player_in_range) shape.setFillColor(sf::Color::Magenta);
		}
        if (being_hurt) shape.setFillColor(sf::Color::Black);
        shape.setSize(size);
        shape.setPosition(position);
        window.draw(shape);
    };

};

struct Bullet : public Entity {
	Bullet(sf::Vector2f nposition, sf::Vector2f ntarget_position) {
		position = nposition;
		target_position = ntarget_position;
		size.x = TILE_SIZE/2;
		size.y = TILE_SIZE/2;
		velocity = 6;
		
		dx = target_position.x - position.x;
		dy = target_position.y - position.y;
		float len = sqrt((dx*dx)+(dy*dy));
		if (len != 0) {
			dx /= len;
			dy /= len;
		}
	};

	sf::Vector2f target_position;
	float dx = 0, dy = 0;
	
	void update() {
		position.x += dx * velocity;
		position.y += dy * velocity;
	};

	void render(sf::RenderWindow &window) {
		sf::RectangleShape shape;
		shape.setFillColor(sf::Color::Red);
		shape.setPosition(position);
		shape.setSize(size);
		window.draw(shape);
	};
};

struct Dungeon {
    Dungeon() {
        gen();
    };

    float width = TILE_SIZE * ROOM_SIZE * DUNGEON_WIDTH;
    float height = TILE_SIZE * ROOM_SIZE * DUNGEON_HEIGHT;

    std::vector<Room*> rooms;
    std::vector<Enemy*> enemies;
	std::vector<Bullet*> bullets;

    bool player_attacked = false;

    sf::Sound hit_effect;
    sf::SoundBuffer hit_effect_buffer;

    void gen() {
    	hit_effect_buffer.loadFromFile("assets/sound_effects/hit.wav");
    	hit_effect.setBuffer(hit_effect_buffer);

        int room_count = rand(6,14);
        bool spawned_player = false;

        for (int i=0; i<room_count; i++) {
            int sector_x = 0;
            int sector_y = 0;

            // Ensure no other rooms in target spot and put a room in it
            while (true) {
                sector_x = rand(0,DUNGEON_WIDTH-1);
                sector_y = rand(0,DUNGEON_HEIGHT-1);

                bool spot_taken = false;
                for (auto r : rooms) {
                    if (floor(r->position.x/TILE_SIZE) == sector_x && floor(r->position.y/TILE_SIZE) == sector_y) {
                        spot_taken = true;
                        break;
                    }
                }

                if (!spot_taken) break;
            }

            rooms.push_back(new Room(((TILE_SIZE*ROOM_SIZE) * sector_x), ((TILE_SIZE*ROOM_SIZE) * sector_y), ROOM_SIZE * TILE_SIZE, ROOM_SIZE * TILE_SIZE));

            // put player in the room
            if ((!spawned_player && rand(1, 4) == 1) || (!spawned_player && i == room_count-1)) {
                println("new player");
                player = new Player(sf::Vector2f(rooms[i]->getPosition().x + (TILE_SIZE*4), rooms[i]->getPosition().y + TILE_SIZE));
                spawned_player = true;
            }
            // put enemy in the room
            enemies.push_back(new Enemy(sf::Vector2f(rooms[i]->getPosition().x + TILE_SIZE, rooms[i]->getPosition().y + (TILE_SIZE*4))));

            enemies.push_back(new Enemy(sf::Vector2f(rooms[i]->getPosition().x + TILE_SIZE, rooms[i]->getPosition().y + (TILE_SIZE*5)), "shooter"));
        }

        // Border walls by creating one large room
        rooms.push_back(new Room(0, 0, DUNGEON_WIDTH, DUNGEON_HEIGHT));
    };

    void update() {
        player->update();

        // Player movement
        player->dx = 0;
        player->dy = 0;
        float len = 0;

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || sf::Keyboard::isKeyPressed(sf::Keyboard::W))
            player->dy = -1;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) || sf::Keyboard::isKeyPressed(sf::Keyboard::S))
            player->dy = 1;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::A))
            player->dx = -1;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || sf::Keyboard::isKeyPressed(sf::Keyboard::D))
            player->dx = 1;

        len = sqrt((player->dx*player->dx)+(player->dy*player->dy));
        if (len !=0) {
            player->dx /= len;
            player->dy /= len;
        }

        player->position.x += player->dx * player->velocity;
        for (auto r : rooms) {
            if (r->getBounds().intersects(player->bounds())) {
                for (auto w : r->walls) {
                    if (player->bounds().intersects(w->bounds())) {
                        player->position.x -= player->velocity * player->dx;
                        break;
                    }
                }
            }
        }
		if (player->position.x < 0) player->position.x -= player->velocity * player->dx;
		if (player->position.x + player->size.x > ROOM_SIZE * DUNGEON_WIDTH * TILE_SIZE) player->position.x -= player->velocity * player->dx;

        player->position.y += player->dy * player->velocity;
        for (auto r : rooms) {
            if (r->getBounds().intersects(player->bounds())) {
                for (auto w : r->walls) {
                    if (player->bounds().intersects(w->bounds())) {
                        player->position.y -= player->velocity * player->dy;
                        break;
                    }
                }
            }
        }

		if (player->position.y < 0) player->position.y -= player->velocity * player->dy;
		if (player->position.y + player->size.y > ROOM_SIZE * DUNGEON_HEIGHT * TILE_SIZE) player->position.y -= player->velocity * player->dy;

        // Player attack enemies and knock them back
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            if (!player_attacked) {
                player_attacked = true;
                Rect attack_bounds(player->target_position.x, player->target_position.y,
                                   player->attack_size,
                                   player->attack_size);
				// Hit enemies
                for (auto e : enemies) {
                    if (e->bounds().intersects(attack_bounds)) {
                        float dx = player->target_position.x - player->position.x;
                        float dy = player->target_position.y - player->position.y;
                        float len = sqrt((dx*dx)+(dy*dy));

                        if (len != 0) {
                            dx /= len;
                            dy /= len;
                        }
                        if (!e->being_hurt) {
                        	hit_effect.play();
                        	e->hurt(1, dx, dy);
                        }
                    }
                }

				// Hit bullets
				for (auto b : bullets) {
					if (b->bounds().intersects(attack_bounds)) {
						b->alive = false;
					}
				}
            }
        } else {
            player_attacked = false;
        }

		// Player hurt by enemies
		for (auto e : enemies) {
			if (!player->being_hurt &&
				e->alive &&
				e->bounds().intersects(player->bounds())) {
				player->hp--;
				player->being_hurt = true;
				player->hurt_timer.reset(true);
				hit_effect.play();
			}
		}

		for (auto b : bullets) {
			if (b->alive) {
				b->update();
				
				// Player hurt by bullets
				if (b->bounds().intersects(player->bounds())) {
					b->alive = false;
					player->hp--;
					player->being_hurt = true;
					player->hurt_timer.reset(true);
					hit_effect.play();
				}
			}
		}
		
		

        // Move enemies
        for (auto e : enemies) {
            if (e->alive) {
                e->update();

				// Check if player is in enemy range
				if (e->type == "shooter") {
					e->player_in_range = player->bounds().intersects(*e->shoot_range);

					// Fire the bullet
					if (e->player_in_range && e->shooting) {
						e->shooting = false;
							e->shot_timer.reset(true);
						bullets.push_back(new Bullet(e->position, player->position));
						println("pew");
					}
				}

                // Move in direction hit in until it hits a wall or meets target
                if (e->being_hurt) {
                    Rect target_hurt_rect(e->target_hurt_position.x, e->target_hurt_position.y, 10, 10);

                    e->position.x += (e->hurt_vector.x * e->knock_back_velocity);
                    for (auto r : rooms) {
                        for (auto w : r->walls) {
                            if (e->bounds().intersects(w->bounds())) {
                                e->position.x -= (e->hurt_vector.x * e->velocity);
                                e->being_hurt = false;
                                break;
                            }
                        }
                    }

                    e->position.y += (e->hurt_vector.y * e->knock_back_velocity);
                    for (auto r : rooms) {
                        for (auto w : r->walls) {
                            if (e->bounds().intersects(w->bounds())) {
                                e->position.y -= (e->hurt_vector.y * e->velocity);
                                e->being_hurt = false;
                                break;
                            }
                        }
                    }

                    if (e->bounds().intersects(target_hurt_rect)) {
                        e->being_hurt = false;
                    }
                }

                if (!e->being_hurt) {
                    // Up
                    if (e->direction == 1) {
                        e->position.y -= e->velocity;

                        for (auto r : rooms) {
                            for (auto w : r->walls) {
                                if (e->bounds().intersects(w->bounds())) {
                                    e->position.y += e->velocity;
                                    while(true) {
                                        int target_direction = rand(1, 4);
                                        if (target_direction != e->direction) {
                                            e->direction = target_direction;
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }

						// Edges of dungeon
						if (e->position.x < 0) {
							e->position.x += e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						if (e->position.x + e->size.x> ROOM_SIZE * DUNGEON_WIDTH * TILE_SIZE) {
							e->position.x -= e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						if (e->position.y < 0) {
							e->position.y += e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						if (e->position.y + e->size.y > ROOM_SIZE * DUNGEON_HEIGHT * TILE_SIZE) {
							e->position.y -= e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						
                    }

                    // Down
                    if (e->direction == 2) {
                        e->position.y += e->velocity;

                        for (auto r : rooms) {
                            for (auto w : r->walls) {
                                if (e->bounds().intersects(w->bounds())) {
                                    e->position.y -= e->velocity;
                                    while(true) {
                                        int target_direction = rand(1, 4);
                                        if (target_direction != e->direction) {
                                            e->direction = target_direction;
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }

						// Edges of dungeon
						if (e->position.x < 0) {
							e->position.x += e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						if (e->position.x + e->size.x > ROOM_SIZE * DUNGEON_WIDTH * TILE_SIZE) {
							e->position.x -= e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						if (e->position.y < 0) {
							e->position.y += e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						if (e->position.y + e->size.y > ROOM_SIZE * DUNGEON_HEIGHT * TILE_SIZE) {
							e->position.y -= e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						

                    }

                    // Left
                    if (e->direction == 3) {
                        e->position.x -= e->velocity;

                        for (auto r : rooms) {
                            for (auto w : r->walls) {
                                if (e->bounds().intersects(w->bounds())) {
                                    e->position.x += e->velocity;
                                    while(true) {
                                        int target_direction = rand(1, 4);
                                        if (target_direction != e->direction) {
                                            e->direction = target_direction;
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }


						// Edges of dungeon
						if (e->position.x < 0) {
							e->position.x += e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						if (e->position.x + e->size.x > ROOM_SIZE * DUNGEON_WIDTH * TILE_SIZE) {
							e->position.x -= e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						if (e->position.y < 0) {
							e->position.y += e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						if (e->position.y + e->size.y > ROOM_SIZE * DUNGEON_HEIGHT * TILE_SIZE) {
							e->position.y -= e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						

                    }

                    // Right
                    if (e->direction == 4) {
                        e->position.x += e->velocity;

                        for (auto r : rooms) {
                            for (auto w : r->walls) {
                                if (e->bounds().intersects(w->bounds())) {
                                    e->position.x -= e->velocity;
                                    while(true) {
                                        int target_direction = rand(1, 4);
                                        if (target_direction != e->direction) {
                                            e->direction = target_direction;
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }


						// Edges of dungeon
						if (e->position.x < 0) {
							e->position.x += e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						if (e->position.x + e->size.x > ROOM_SIZE * DUNGEON_WIDTH * TILE_SIZE) {
							e->position.x -= e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						if (e->position.y < 0) {
							e->position.y += e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						if (e->position.y + e->size.y > ROOM_SIZE * DUNGEON_HEIGHT * TILE_SIZE) {
							e->position.y -= e->velocity;
							while(true) {
								int target_direction = rand(1, 4);
								if (target_direction != e->direction) {
									e->direction = target_direction;
									break;
								}
							}
						}
						

                    }
                }
            }
        }
    };

    void render(sf::RenderWindow &window) {

        sf::RectangleShape shape;
        shape.setFillColor(sf::Color(80, 80, 80, 255));
        shape.setSize(sf::Vector2f(width, height));
        window.draw(shape);

        Rect camera_bounds(camera.getCenter().x - (camera.getSize().x/2),
                           camera.getCenter().y - (camera.getSize().y/2),
                           camera.getSize().x,
                           camera.getSize().y);


        for (auto r : rooms) {
            if (r->getBounds().intersects(camera_bounds)) {
                sf::RectangleShape room_shape;
                room_shape.setFillColor(sf::Color(140, 140, 140, 255));
                room_shape.setSize(r->getSize());
                room_shape.setPosition(r->getPosition());
                room_shape.setOutlineThickness(2);
                room_shape.setOutlineColor(sf::Color::Black);
                window.draw(room_shape);

                r->render(window);
            }
        }

        for (auto e : enemies) {
            if (e->alive) e->render(window);
        }

        player->render(window);


		for (auto b : bullets) {
			if (b->alive) b->render(window);
		}
    };
};

Dungeon dungeon;

void init() {

}

void update() {
    dungeon.update();
}

void render() {
    window.clear(sf::Color::Black);

    dungeon.render(window);
}

void input() {

}

int main() {
    window.create(sf::VideoMode(1280, 720), "Game", sf::Style::Titlebar);
    window.setFramerateLimit(60);
    camera.setSize(1280, 720);
    //camera.zoom(.8f);
    window.setView(camera);

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
        }
        window.setView(camera);
        input();
        update();
        render();
        window.display();
    }
    return 0;
}
