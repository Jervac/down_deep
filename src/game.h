#ifndef GAME_H
#define GAME_H

struct Game {
    void init();
    void input();
    void update();
    void render();
};
#endif