#include "super_file.h"
#include <fstream>
#include <iostream>
#include <sstream>

SuperFile::SuperFile(std::string nfile) {
	file = nfile;
}

// --------------------------------------------
// TODO: Have the loader ignore commented files
// --------------------------------------------
void SuperFile::hotload() {
	std::ifstream f(file);

	if (f) {
		file_contents.clear();
		std::string current_line;
		
		while (std::getline(f, current_line)) {
			file_contents.push_back(current_line);
		}

		// Prints contents loaded from file
		if (debug) {
			std::cout 
				<< "------------------------------------------------------\n"
				<< "[DEBUG]: Hotloaded [" << file << "]:" 
				<< std::endl;

			for (int i=0; i<file_contents.size(); i++) {
				std::cout << file_contents[i] << std::endl;
			}
			std::cout 
				<< "------------------------------------------------------"
				<< std::endl;
		}
		
	} else {
		std::cout << "[ERROR] Couldn't load [" << file << "]" << std::endl;
	}
	f.close();
}

std::string SuperFile::getItem(std::string target_variable, int target_index) {
	std::ifstream f(file);
	std::string current_line;

	if (f) {
		while (std::getline(f, current_line)) {	
			std::vector<std::string> results;
			std::stringstream data(current_line);
			std::string current_item;
			
			while (std::getline(data, current_item, delimiter)) {
				results.push_back(current_item);
			}

			if (results[0] == target_variable) {
				return results[target_index+1];
			}
		}
	} else {
		std::cout << "[ERROR] Couldn't load [" << file << "]" << std::endl;
	}
	f.close();
	return NULL;
}

void SuperFile::clearFile() {
	file_contents.clear();
	std::ofstream ofs;

	// trunc flag deletes contents of the file
	ofs.open(file, std::ofstream::out | std::ofstream::trunc);
	ofs.close();
}

void SuperFile::addString(std::string text) {
	file_contents.push_back(text);
	std::ofstream ofs;
	
	// app appends text to the filex
	ofs.open(file, std::ofstream::out | std::ofstream::app);
	ofs << text;
	ofs.close();
}
