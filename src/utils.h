#ifndef UTILS_H
#define UTILS_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>

////////////////////////////////////////
// Utilities to simplify code verbosity
////////////////////////////////////////

template <typename T>
inline void println(T const& t) {
	std::cout << t << std::endl;
};

int rand(int min, int max);

struct Rect {
	Rect(float nx, float ny, float nwidth, float nheight);
	float x = 0, y = 0;
	float width = 0, height = 0;
	bool intersects(Rect b);
	sf::Vector2f getPosition();
	void setPosition(sf::Vector2f nposition);
	sf::Vector2f getSize();
	void setSize(sf::Vector2f nsize);
};
#endif
