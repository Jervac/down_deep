#include "utils.h"
#include <random>

Rect::Rect(float nx, float ny, float nwidth, float nheight) {
    x = nx;
    y = ny;
    width = nwidth;
    height = nheight;
}


// TODO: have function to return size of overlap
bool Rect::intersects(Rect b) {
    if(x >= b.x + b.width) {
        return false;
    }
    if(x + width <= b.x) {
        return false;
    }
    if(y >= b.y + b.height) {
        return false;
    }
    if(y + height <= b.y) {
        return false;
    }
    return true;
}

sf::Vector2f Rect::getPosition() {
    return sf::Vector2f(x, y);
}

void Rect::setPosition(sf::Vector2f nposition) {
    x = nposition.x;
    y = nposition.y;
}

sf::Vector2f Rect::getSize() {
    return sf::Vector2f(width, height);
}

void Rect::setSize(sf::Vector2f nsize) {
    width = nsize.x;
    height = nsize.y;
}

/*
int rand(int min, int max) {
    static bool first = true;
    if (first) {
        std::srand(time(NULL));
        first = false;
    }
    return std::rand() % max + min;
}
*/

int rand(int min, int max) {
	const int range_from  = min;
	const int range_to    = max;
    std::random_device                  rand_dev;
    std::mt19937                        generator(rand_dev());
    std::uniform_int_distribution<int>  distr(range_from, range_to);
	return distr(generator);
}
